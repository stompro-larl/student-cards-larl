# student-cards

PINES student card import utilities from school information management systems.

## Basic Usage

    ./import_student_data.pl

To be run on a server with direct access to the PostgreSQL server running Evergreen-ILS.

## Background/Assumptions

This project has been developed to support a PINES library system who wishes to have
an automated process for importing student card accounts into PINES.  In this context
"student card" refers to a restricted patron account providing basic access to students
within a school district.

In this project, each school district is the parent unit over a set of school units divided
by grade level (e.g., Pre-K, Elementary, Middle School, High School) which are geographically
located near particular library branches.  The library system structure is similar, with a 
"System" as the parent unit over its "Branches".

This script assumes the presence of a "Student Card" permissions profile within Evergreen.

## Installation

Run `student_card_db_schema.sql` with psql to create the required schema in the database.

The script is intended to run nightly via a cron job on a utility server.

## Setup

On a server with a direct connection to the database server, create a `~/.pgpass` file
with the connection information and credentials for the PostgreSQL instance.  Then,
create a `~/.pg_service.conf` file with something like the following:

    [evergreen]
    dbname=evergreen
    user=evergreen
    host=localhost

where `dbname` is the name of the database you're connecting to as `user` on `host`.

### Importing District and School Data

Each district will need a server connection like secure FTP or SCP (preferred) that is made 
available to the Evergreen instance.  Connection information is stored within the database.
Currently, only explicit FTP over TLS has been implemented.

To add a school district to the database, do the following:

    insert into student_card.district (
        code, 
        name, 
        contact_name, 
        contact_email,
        ftp_host,
        ftp_user,
        ftp_pass,
        ftp_remote_dir
    ) values (
        'MSD', -- this code will become the prefix to the student card barcode
        'My School District',
        'Jane Admin',
        'sysadmin@myschooldistrict.com',
        'sftp://ftp.myschooldistrict.com', -- or "ftp://" or "ftpes://"
        'myftpuser',
        'myftppass',
        '/my/remote/filepath'
    );

The `code` value will be used for naming the remote export files and for prefixing the 
student card account barcode.

`ftp_host` needs to be in a URI format.  FTP, SFTP, and Explicit FTP over SSL (FTPES) are
currently supported.

`name`, `contact_name`, and `contact_email` are visible only to Evergreen database
administrators.

To add the schools, insert each with something like the following:

    insert into student_card.school (
        district_id,
        name,
        state_id,
        grades,
        addr_street_1,
        addr_street_2,
        addr_city,
        addr_county,
        addr_state,
        addr_post_code,
        eg_perm_group,
        home_ou
    ) values (
        (select id from student_card.district where code = 'MSD'),
        "My School"
        1234,
        '9-12',
        '123 Any Avenue',
        'Suite 200',
        'My City',
        'My County',
        'My State',
        '12345'
        (select id from permission.grp_tree where name = 'Student Card'),
        (select id from actor.org_unit where shortname = 'BR1')
    );

`district_id` (integer) will refer to the `id` field on `student_card.district`.

`name` (text) will be added to the "Secondary Identification" field within the user account.

`state_id` (integer) is used to match student data within an import file to the correct school.

`grades` (text) and address data (text) is only visible to Evergreen database administrators.

`eg_perm_group` (integer) refers to the ids of available profiles in `permission.grp_tree`.

`home_ou` (integer) refers to the ids of organizational units in `actor.org_unit`. Deciding
which library should be assigned to which school is up to the local library/school systems.
       

## Import File Format

Import files will need to be in CSV format with the following fields and header names in the
following order:

    School_ID (State-assigned numeric code)
    Student_ID (District-assigned unique ID)
    Student_FName
    Student_MName (Optional)
    Student_LName
    Student_DOB (YYYY-MM-DD)
    Student_Phone (555-555-5555)
    Student_Email (Optional, but highly recommended to allow automated notices - this could also be the parent/guardian's email address)
    Address_Street1
    Address_Street2 (Optional)
    Address_City
    Address_County
    Address_State
    Address_Postal
    Parent_Guardian (Optional)
    Grade (Optional)

Files must have the naming convention of the district code and a date string `PRE_YYYYMMDDHHMM`.  For example:

    MSD_202002111037

would be a file created by the MSD school district on February 11, 2020 at 10:37 a.m.


## Imports

Imports will be performed nightly via cron.  The script will log into each configured district server and download any files with the
proper naming convention that have not already been downloaded.  Processed file names are recorded in the `student_card.import` table.

The import script checks each row in the CSV file, generates the library card barcode (district code + student ID) and checks
whether that barcode already exists in the Evergreen database.  If the barcode does not yet exist, the script creates the account with
the row data.  If the barcode exists, the script will update selected fields in the account with the import file's data.

## Field Mappings

Below are the mappings between the CSV Fields and the destination Evergreen database table columns.

- **School_ID** is used to match the student to the school information
- **Student_ID** along with the district code, becomes the library card barcode and is also available in `actor.usr.ident_value2`
- **Student_FName** becomes the patron first name (`actor.usr.first_given_name`)
- **Student_MName** becomes the patron middle name (`actor.usr.second_given_name`)
- **Student_LName** becomes the patron last name (`actor.usr.family_name`)
- **Student_DOB** becomes the patron date of birth and is used to generate the initial patron password (`actor.usr.dob` and `actor.usr.passwd`)
- **Student_Phone** becomes the patron phone number
- **Student_Email** becomes `actor.usr.email`
- **Address_Street1** becomes the first street entry on the patron mailing address (`actor.usr_address.street1`)
- **Address_Street2** becomes the second street entry on the patron mailing address (`actor.usr_address.street2`)
- **Address_City** becomes the city on the patron mailing address (`actor.usr_address.city`)
- **Address_County** becomes the county on the patron mailing address (`actor.usr_address.county`) (school county is used if county is not provided)
- **Address_State** becomes the state on the patron mailing address (`actor.usr_address.state`)
- **Address_Postal** becomes the ZIP code on the patron mailing address (`actor.usr_address.post_code`)
- **Parent_Guardian** becomes the parent/guardian field (`actor.usr.guardian`).  If empty, the school name is entered in this field.
- **Grade** This is appended to "grade" and entered in the Name Keywords field (`actor.usr.name_keywords`)

