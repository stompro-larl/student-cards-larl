#!/usr/bin/perl

use warnings;
use strict;
use DBI;
use Text::CSV qw/ csv /;

# Get district info from the command line...

my @district_data;

print "Please enter the district name: ";
chomp(my $dist_name = <STDIN>);
print "Please enter the district prefix/code: ";
chomp(my $dist_code = <STDIN>);
print "Please enter the district's State ID: ";
chomp(my $dist_id = <STDIN>);

# Set up CSV
my $csvfile = $ARGV[0];

my $school_data = csv (in => "$csvfile", headers => "lc", empty_is_undef => 1)
	or die Text::CSV->error_diag;

# Set up DBI
my $settings = {
    host => "localhost",
    db => "evergreen",
    user => "evergreen"
};

my $dbh = DBI->connect('DBI:Pg:dbname=' . $settings->{'db'} . ';host=' . $settings->{'host'}, $settings->{'user'},
            undef,
            {
                RaiseError => 1,
                ShowErrorStatement => 0,
                AutoCommit => 0
            }
) or die DBI->errstr;

my $district_query = qq/ insert into student_card.district
			 (code,
			  name,
			  state_id
			  ) values (
			  ?,
			  ?,
			  ?) /;

my $school_query = qq/	insert into student_card.school (
			district_id,
			name,
			state_id,
			grades,
			addr_street_1,
			addr_city,
			addr_county,
			addr_state,
			addr_post_code,
			eg_perm_group,
			home_ou
		  	) values (
			(select id from student_card.district where code = ?),
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			(select id from permission.grp_tree where name = 'PLAY Card'),
			(select id from actor.org_unit where shortname = ?)
			) /;


my $sth = $dbh->prepare($district_query);
$sth->execute($dist_code, $dist_name, $dist_id);

for my $data (@$school_data) {
	my @school_fields = (	
			$dist_code,
			$data->{'school name'},
			$data->{'school id'},
			$data->{'grades'},
			$data->{'address1'},
			$data->{'address_city'},
			$data->{'county'},
			$data->{'address_state'},
			$data->{'address_postal'},
			$data->{'home library'}
		);



	$sth = $dbh->prepare($school_query);
	$sth->execute(@school_fields);
}

$dbh->commit;
$sth->finish;
$dbh->disconnect;
