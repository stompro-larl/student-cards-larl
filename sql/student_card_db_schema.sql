BEGIN;

DROP SCHEMA student_card CASCADE;

CREATE SCHEMA student_card;

CREATE TABLE student_card.district (
    id                  SERIAL PRIMARY KEY,
    code                TEXT NOT NULL UNIQUE,
    name                TEXT NOT NULL UNIQUE,
    active              BOOLEAN NOT NULL DEFAULT TRUE,
    state_id            INTEGER NOT NULL UNIQUE,
    contact_name        TEXT,
    contact_email       TEXT,
);

CREATE TABLE student_card.school (
    id                  SERIAL PRIMARY KEY,
    district_id         INTEGER NOT NULL REFERENCES student_card.district (id),
    name                TEXT NOT NULL,
    state_id            INTEGER NOT NULL, -- school ID per the State of Georgia
    grades              TEXT,
    addr_street_1       TEXT,
    addr_street_2       TEXT,
    addr_city           TEXT,
    addr_county         TEXT,
    addr_state          TEXT,
    addr_post_code      TEXT,
    eg_perm_group       INTEGER NOT NULL REFERENCES permission.grp_tree (id),
    home_ou             INTEGER NOT NULL REFERENCES actor.org_unit (id)
);
CREATE UNIQUE INDEX student_card_school_state_id_idx ON student_card.school (district_id, state_id);
CREATE UNIQUE INDEX student_card_school_name_once_per_district_idx ON student_card.school (district_id, name);

CREATE TABLE student_card.import (
    id                  SERIAL PRIMARY KEY,
    import_time         TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    district_id         INTEGER NOT NULL REFERENCES student_card.district (id),
    filename            TEXT,
    error_message       TEXT
);

COMMIT;
